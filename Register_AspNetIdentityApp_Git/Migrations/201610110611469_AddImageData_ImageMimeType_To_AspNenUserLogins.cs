namespace Register_AspNetIdentityApp_Git.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddImageData_ImageMimeType_To_AspNenUserLogins : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUserLogins", "ImageData", c => c.Binary());
            AddColumn("dbo.AspNetUserLogins", "ImageMimeType", c => c.String());
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUserLogins", "Image", c => c.String());
            DropColumn("dbo.AspNetUserLogins", "ImageMimeType");
        }
    }
}
