namespace Register_AspNetIdentityApp_Git.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class WorkUserName : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AspNetUsers", "MyUserName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "MyUserName", c => c.String());
        }
    }
}
