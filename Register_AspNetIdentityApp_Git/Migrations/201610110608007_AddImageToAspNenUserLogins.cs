namespace Register_AspNetIdentityApp_Git.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddImageToAspNenUserLogins : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUserLogins", "Image", c => c.String());
            AddColumn("dbo.AspNetUserLogins", "Discriminator", c => c.String(nullable: false, maxLength: 128));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUserLogins", "Discriminator");
            DropColumn("dbo.AspNetUserLogins", "Image");
        }
    }
}
