﻿using System.ComponentModel.DataAnnotations;

namespace Register_AspNetIdentityApp_Git.Models
{
    public class PasswordForgotModel
    {
        [Required(ErrorMessage = "Необходимо ввести E-mail")]
        [Display(Name = "Введите E-mail:")]
        [EmailAddress(ErrorMessage = "Неправильный тип Email")]
        public string Email { get; set; }
    }
}