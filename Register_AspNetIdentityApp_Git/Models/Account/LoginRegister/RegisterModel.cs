﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Register_AspNetIdentityApp_Git.Models
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "Необходимо ввести E-mail")]
        [EmailAddress(ErrorMessage = "Неверный формат E-mail")]
        [Remote("ValidateEmailRegister", "Home", HttpMethod = "POST")]
        public string Email { get; set; }

        public string MyUserName { get; set; }

        [Required(ErrorMessage = "Необходимо ввести Пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Необходимо подтвердить пароль")]
        [DataType(DataType.Password)]
        [Remote("ValidatePasswordConfirmRegister", "Home", HttpMethod = "POST", AdditionalFields = "Password,PasswordConfirm")]
        //[System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Пароли не совпадают")]
        public string PasswordConfirm { get; set; }
    }
}

