﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Register_AspNetIdentityApp_Git.Models
{
    public class VkLoginConfirmEmailModel
    {
        [Required(ErrorMessage = "Необходимо ввести E-mail")]
        [EmailAddress(ErrorMessage = "Неверный формат E-mail")]
        [Remote("ValidateEmailRegister", "Home", HttpMethod = "POST")]
        public string Email { get; set; }

        public string MyUserName { get; set; }

        public string idVkUser { get; set; }

        public byte[] ImageDataMy { get; set; }
    }
}