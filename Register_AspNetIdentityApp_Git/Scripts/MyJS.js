﻿function OnSuccess() {
    var backUrlLink = $('#UrlAddress').val();
    $(location).attr('href', backUrlLink);
}

/*
<script>
$(document).ready(function () {
    $("#buttonLogIn").on("click", function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            data: $("#FormLogIn").serialize(),
            url: '@Url.Action("Login", "Account")',
            success: function () {
                var text = document.getElementById('UrlAddress').value;
                $(location).attr('href', text);
            }
        });
    });
});
</script>
*/
// #endregion
//var backUrlLink = document.getElementById('UrlAddress').value;