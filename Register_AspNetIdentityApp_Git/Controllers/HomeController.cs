﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Register_AspNetIdentityApp_Git.Models;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Register_AspNetIdentityApp_Git.Controllers
{
    public class HomeController : Controller
    {
        public ApplicationUserManager UserManager // // // регистрация
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }
        
        
        public ActionResult Index()
        {
            return View();
        }
        

        public ActionResult testIndex()
        {
            return View();
        }

        // // //Удаленная проверка. http://professorweb.ru/my/ASP_NET/mvc/level7/7_6.php
        [HttpPost]
        public JsonResult ValidateEmailRegister(string Email) //В дополнение к возврату такого результата, методы действий для проверки достоверности должны определять параметр, который имеет то же самое имя, что и проверяемое поле данных: в рассматриваемом примере это Date. 
        {
            ApplicationUser existEmail = new ApplicationUser(); //Email
            existEmail = UserManager.FindByEmail(Email);
            if (existEmail != null) //есть
                return Json("Client: Такой Email, уже есть!", JsonRequestBehavior.AllowGet);
            else //нету
                return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ValidateEmailLogin(string Email) //В дополнение к возврату такого результата, методы действий для проверки достоверности должны определять параметр, который имеет то же самое имя, что и проверяемое поле данных: в рассматриваемом примере это Date. 
        {
            ApplicationUser existEmail = new ApplicationUser(); //Email
            existEmail = UserManager.FindByEmail(Email);
            if (existEmail != null) //есть
                return Json(true, JsonRequestBehavior.AllowGet);
            else //нету
                return Json("Client: Такого Email'a нет!", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ValidatePasswordConfirmRegister(string Password, string PasswordConfirm) //В дополнение к возврату такого результата, методы действий для проверки достоверности должны определять параметр, который имеет то же самое имя, что и проверяемое поле данных: в рассматриваемом примере это Date. 
        {
            if (Password == PasswordConfirm)
                return Json(true, JsonRequestBehavior.AllowGet);
            else 
                return Json("Client: Пароли не совпадают!", JsonRequestBehavior.AllowGet);
        }
    }
}