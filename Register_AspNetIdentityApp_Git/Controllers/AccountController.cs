﻿using System.Web;
using System.Web.Mvc;
using Register_AspNetIdentityApp_Git.Models;
using Microsoft.AspNet.Identity; // // // регистрация
using Microsoft.AspNet.Identity.Owin; // // // регистрация
using Microsoft.Owin.Security; // // // логин + логаунт
using System.Security.Claims; // // // логин + логаунт
using System.Net;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;

namespace Register_AspNetIdentityApp_Git.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        string DefaultAvatarPath = AppDomain.CurrentDomain.BaseDirectory + "Content\\Images\\Avatars\\default_user.jpg";
        public ApplicationUserManager UserManager // // // регистрация
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        public IAuthenticationManager AuthenticationManager // // // логин + логаунт
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        // // // логаут
        [AllowAnonymous]
        public ActionResult LogOut(string pathView)
        {
            AuthenticationManager.SignOut();
            return Redirect(pathView);
        }

        // // // Подтверждение Email
        [AllowAnonymous]
        public ActionResult EmailConfirm(string userId, string code)
        {
            if (userId == null || code == null)
            {
                ModelState.AddModelError("", "Неправильные userId или code");
                return PartialView("~/Views/Account/LoginRegister/Register.cshtml");
            }

            //Вдруг удалится пользователь, пока мы долго подтверждали E-mail в письме или просрочится токен доступа, поэтому лучше сначала проверить есть ли такой пользователь.
            ApplicationUser user = UserManager.FindById(userId); //ищем в базе
            if (user == null) // если нету
            {
                ModelState.AddModelError("NoUserToDb", "Ошибка подтверждения E-mail. Такого пользователя уже нет в базе! Необходимо повторить регистрацию!");
                return View("~/Views/Account/LoginRegister/LoginRegister.cshtml");
            }

            IdentityResult result = UserManager.ConfirmEmail(userId, code);
            if (result.Succeeded)
            {
                ClaimsIdentity claim = UserManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
                AuthenticationManager.SignOut(); // удаляет авторизационные куки.
                AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = true }, claim); // создает авторизационные куки.
                return View("~/Views/Account/Email/EmailConfirm.cshtml");
            }
            else
            {
                ModelState.AddModelError("", "Bad Token");
                return PartialView("~/Views/Account/LoginRegister/Register.cshtml");
            }
        }

        [AllowAnonymous]
        public ActionResult EmailShow()
        {
            return PartialView("~/Views/Account/Email/EmailShow.cshtml");
        }

        // // // логин
        [AllowAnonymous]
        public ActionResult Login()
        {
            return RedirectToAction("LoginRegister", "Account");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid) //модель валидна
            {
                // // // Проверки на уникальность:
                //1. Проверочка, если пытаемся ввести E-mail которого нет!
                ApplicationUser existEmail = new ApplicationUser(); //Email
                existEmail = UserManager.FindByEmail(model.Email);
                if (existEmail == null)
                {
                    ModelState.AddModelError("Email", "Server: Данный Email отсутствует!");
                    return PartialView("~/Views/Account/LoginRegister/Login.cshtml");
                }
        
                //2. Если есть юзер с таким Email, проверяем Логин/Пароль!
                ApplicationUser existLoginPassword = new ApplicationUser(); //UserName 
                string userLogin = existEmail.UserName; //взяли у найденного выше по Email пользователя, поле UserName - для метода Find(UserName, Password).
                existLoginPassword = UserManager.Find(userLogin, model.Password);
                if (existLoginPassword == null)
                    ModelState.AddModelError("Password", "Неправильный логин/пароль");

                //3. Подтвержден ли Email?
                if (existEmail.EmailConfirmed != true)
                    ModelState.AddModelError("Email", "Не подтвержден E-mail");

                //4. Если есть ошибки, то заново
                var modelStateErrors = this.ModelState.Keys.SelectMany(key => this.ModelState[key].Errors).ToList();
                if (modelStateErrors.Count > 0)
                    return PartialView("~/Views/Account/LoginRegister/Login.cshtml");

                //5.1. переназначить общие значения для кук
                HttpContext.Response.Cookies["AvatarPath"].Value = DefaultAvatarPath; // подменяем путь аватарки на стнд.
                HttpContext.Response.Cookies["first_name_last_name"].Value = existEmail.MyUserName; //отображение имени на главной

                //5.2. Если все ок, Логинимся.
                ClaimsIdentity claim = UserManager.CreateIdentity(existEmail, DefaultAuthenticationTypes.ApplicationCookie);
                AuthenticationManager.SignOut(); // удаляет авторизационные куки.
                AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = true }, claim); // создает авторизационные куки.

                return JavaScript("OnSuccess();"); //переходим на ту страницу, с которой пришли.
            }
            else
                return PartialView("~/Views/Account/LoginRegister/Login.cshtml");
        }

        // // // Общая Вью, для отображения ПаршелВью Логин и Регистрации + запоминает адрес с которого был запрос!
        [AllowAnonymous]
        public ActionResult LoginRegister(string pathView) //передаем путь страницы с которой был совершен переход на Логин/Пароль.
        {
            if (HttpContext.User.Identity.IsAuthenticated) //Если залогинен и заходишь вручную по адресу Account/LoginRegister, чтобы одновременно не видеть LogIn и LogOut.
                return RedirectToAction("Index", "Home");

            if (pathView == null) //если старт с пустой страницы, то идем на главную
                pathView = Url.Action("Index", "Home");

            UrlView urlView = new UrlView(); //запоминаем страницу с которой пришли
            urlView.UrlAddress = pathView;

            //сохраняем url в куки.
            HttpContext.Response.Cookies["pathView"].Value = pathView;

            return View("~/Views/Account/LoginRegister/LoginRegister.cshtml", urlView);
        }

        // // // Восстановление пароля
        [AllowAnonymous]
        public ActionResult PasswordForgot()
        {
            return View("~/Views/Account/PasswordForgotReset/PasswordForgot.cshtml");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult PasswordForgot(PasswordForgotModel model) //ввод электронного адреса для сброса пароля
        {
            if (ModelState.IsValid)
            {
                var user = UserManager.FindByName(model.Email);
                if (user == null || !(UserManager.IsEmailConfirmed(user.Id)))
                {
                    ModelState.AddModelError("", "Пользователь не подтвержден, либо его не существует.");
                    return View("~/Views/Account/PasswordForgotReset/PasswordForgot.cshtml");
                }

                string code = UserManager.GeneratePasswordResetToken(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                /*???*/
                UserManager.SendEmail(user.Id, "Сброс пароля", "Для сброса пароля, перейдите по ссылке <a href=\"" + callbackUrl + "\">сбросить</a>");
                return View("~/Views/Account/PasswordForgotReset/ForgotPasswordConfirmation.cshtml");
            }
            return View("~/Views/Account/PasswordForgotReset/PasswordForgot.cshtml", model);
        }

        //После отправки письма пользователь перенаправляется к методу PasswordForgotConfirmation. 
        //Все остальные действия по сбросу пароля выполняет метод PasswordReset,
        [AllowAnonymous]
        public ActionResult PasswordReset(string code)
        {
            if (code == null)
                return View("Error");
            return View("~/Views/Account/PasswordForgotReset/PasswordReset.cshtml");
        }

        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult PasswordReset(PasswordResetModel model)
        {
            if (ModelState.IsValid)
            {
                var user = UserManager.FindByName(model.Email);
                if (user == null)
                {
                    ModelState.AddModelError("", "Такого пользователя не найдено.");
                    return View("~/Views/Account/PasswordForgotReset/PasswordReset.cshtml");
                }
                IdentityResult result = UserManager.ResetPassword(user.Id, model.Code, model.Password);
                if (result.Succeeded)
                {
                    ClaimsIdentity claim = UserManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
                    AuthenticationManager.SignOut(); // удаляет авторизационные куки.
                    AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = true }, claim); // создает авторизационные куки.

                    return View("~/Views/Account/PasswordForgotReset/PasswordResetConfirmation.cshtml");
                }
                else
                {
                    foreach (string error in result.Errors)
                        ModelState.AddModelError("", error);
                    return View("~/Views/Account/PasswordForgotReset/PasswordReset.cshtml");
                }
            }
            return View("~/Views/Account/PasswordForgotReset/PasswordReset.cshtml", model);
        }

        // // // регистрация
        [AllowAnonymous]
        public ActionResult Register()
        {
            return RedirectToAction("LoginRegister", "Account");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                //Из метода Регистрацци. Ненужный, но полезный код, т.к. сейчас работает RemoteValidation
                // // // Проверки на уникальность:
                //1. Проверочка, если пытаемся ввести уже существующий E-mail!!!
                ApplicationUser existEmail = new ApplicationUser(); //Email
                existEmail = UserManager.FindByEmail(model.Email);
                if (existEmail != null)
                    ModelState.AddModelError("Email", "Server: Такой пользователь уже есть!");

                //2. Если есть ошибки, то заново. 
                var modelStateErrors = ModelState.Keys.SelectMany(key => this.ModelState[key].Errors).ToList();
                if (modelStateErrors.Count > 0)
                    return PartialView("~/Views/Account/LoginRegister/Register.cshtml");
                               
                //*если поле никнейма - пустое, тогда по дефолту присвоить никнейму - емейл адрес!
                string tempMyUserName = model.MyUserName;
                if (tempMyUserName == null)
                    tempMyUserName = model.Email;
                
                // Если все ок по принятым данным на сервер от клиента, создаем нового пользователя!!! (Eсли нету Email в таблице)
                ApplicationUser user = new ApplicationUser { Email = model.Email, UserName = model.Email, MyUserName = tempMyUserName }; //Создаем пользователя с одинаковыми полями Email и UserName, тк. по дефолту UserName - должен быть уникальным полем и часть функций OWIN заточены под UserName! Для Никнейма проще создать новое поле.
                IdentityResult result = UserManager.Create(user, model.Password);
                if (result.Succeeded)
                {
                    var code = UserManager.GenerateEmailConfirmationToken(user.Id); //генерим токен для подтверждения регистрации
                    var callbackUrl = Url.Action("EmailConfirm", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme); //создаем ссылку для подтверждения

                    string textEmail = "<p><a href=\"" + callbackUrl + "\">Нажмите</a> для завершения регистрации учетной записи <b>" + user.MyUserName + "</b></p>"; //Текст письма

                    try //если письмо отправилось, значит сервер не в спаме и Email существует!
                    { 
                        UserManager.SendEmail(user.Id, "Подтверждение электронной почты для " + tempMyUserName, textEmail);  //шлем письмо
                    }
                    catch (SmtpException ex) //нет такого Email, или сработала защита от спама на серверном Email'е.
                    {
                        SmtpStatusCode statusCode = ex.StatusCode;
                        if (statusCode == SmtpStatusCode.MailboxBusy || statusCode == SmtpStatusCode.MailboxUnavailable || statusCode == SmtpStatusCode.TransactionFailed)
                        {
                            UserManager.Delete(user); //удаляем вновь созданного пользователя.
                            return PartialView("~/Views/Account/Email/EmailError.cshtml"); 
                        }
                    }

                    return RedirectToAction("EmailShow", "Account");
                }
                else //не создаст Юзера
                {
                    foreach (string error in result.Errors)
                        ModelState.AddModelError("DontCreateUser", error);
                    return PartialView("~/Views/Account/LoginRegister/Register.cshtml");
                }
            }
            else
                return PartialView("~/Views/Account/LoginRegister/Register.cshtml");
        }

        // // // Авторизация через внешние сервисы.
        //1. VK
        private string clientId = "5640201"; //ID приложения.
        private string clientSecret = "PCMEJyd77XMMyeAu5miP"; //Защищенный ключ приложения.
        private string redirectUri = "http://localhost:12566/Account/VkConfirmCodeToken"; //обратный вызов метода с клиента на сервере!! (подтвердить code)
        private string scope = "email"; //что нужно от приложения

        private string VkProviderKey = "VK";

        [AllowAnonymous]
        public ActionResult VKAuthCode() //1. генерим get-запрос на отсылку для vk, и получения code
        {
            //Шлем ID приложения, получаем CODE.
            string url_Code = "https://oauth.vk.com/authorize?client_id=" + clientId + "&display=page&redirect_uri=" + redirectUri + "&scope=" + scope + "&response_type=code&v=5.57"; //v=5.53
            return Redirect(url_Code);
        }

        [AllowAnonymous]
        public ActionResult VkConfirmCodeToken(string code) //2. Подтверждение code (метод записан в приложении в ВК!!!)
        {
            string tokenJSON;
            string userInfoJSON;

            //1. Готовим ссылку для получения токена.
            string url_Token = "https://oauth.vk.com/access_token?client_id=" + clientId + "&client_secret=" + clientSecret + "&redirect_uri=" + redirectUri + "&code=" + code; //Адрес для отправки Токена с полученным Code.

            //2. Переходим по ссылке, и сохраняем полученный ответ.
            using (WebClient webClientToken = new WebClient()) //Делаем запрос от сервера на адрес "url_Token" / Шлем ID и СекретныйКлючПрилож, получаем access_token.
            {
                tokenJSON = webClientToken.DownloadString(url_Token); //ответ {"access_token":"4877fcd353ed0c2ffc54d502b8848122148d920101c71d4cfeb89b287e8a34ab9f97fdb4ef85f48148904","expires_in":86397,"user_id":3651939,"email":"yanlinkin@gmail.com"}
            }

            //3. Распаршиваем полученный JSON и достаем access_token из ответа.                                   
            JObject answerJSON = JObject.Parse(tokenJSON);
            string VkToken = Convert.ToString(answerJSON["access_token"]);
            string VkLoginProvider = Convert.ToString(answerJSON["user_id"]); //id страницы в ВК, а не пользователя из гл табл!!!
            string VkEmail = Convert.ToString(answerJSON["email"]);

            //1. Готовим ссылку для получения FName и LName юзев из ВК и аву!
            string url_userInfo = "https://api.vk.com/method/users.get?user_id=" + VkLoginProvider + "&v=5.57&access_token=" + VkToken + "&fields=photo_200&lang=en";

            //2. Переходим по ссылке, и сохраняем полученный ответ.
            using (WebClient webClientToken = new WebClient()) //Делаем запрос от сервера на адрес "url_Token" / Шлем ID и СекретныйКлючПрилож, получаем access_token.
            {
                userInfoJSON = webClientToken.DownloadString(url_userInfo); //ответ с массивом! : {"response":[{"id":3651939,"first_name":"Yan","last_name":"Popkov","photo_200":"http:\/\/cs636....cuA.jpg"}]}
            }

            //3. Получим вложенный массив отдельно.
            answerJSON = JObject.Parse(userInfoJSON);
            string tempAnswer2 = Convert.ToString(answerJSON.SelectToken("response"));
            /*теперь ответ такой:
            [
                {
                    "id": 3651939,
                    "first_name": "Yan",
                    "last_name": "Popkov"
                }
            ]
            */

            //4. Получим отдельные свойства! http://stackoverflow.com/questions/15726197/parsing-a-json-array-using-json-net
            Dictionary<string, string> dictionaryAnswer2 = new Dictionary<string, string>();
            JArray a = JArray.Parse(tempAnswer2);
            foreach (JObject o in a.Children<JObject>())
                foreach (JProperty p in o.Properties())
                    dictionaryAnswer2.Add(p.Name, (string)p.Value);

            //5. Достанем из словаря нужные свойства!
            string FName = dictionaryAnswer2["first_name"];
            string LName = dictionaryAnswer2["last_name"];
            string VkMyUserName = FName + " " + LName; //полный никнейм из ВК.

            string VkAvatarUrl = dictionaryAnswer2["photo_200"]; //url картинки
            string VkAvatarPath = AppDomain.CurrentDomain.BaseDirectory + "Content\\Images\\Avatars\\VK\\id" + VkLoginProvider + ".jpg";
            
            //сохраняем ответы в куки.  
            //HttpContext.Response.Cookies["access_token"].Value = VkToken;  
            //HttpContext.Response.Cookies["user_id"].Value = VkLoginProvider; 
            HttpContext.Response.Cookies["first_name_last_name"].Value = VkMyUserName; 
            HttpContext.Response.Cookies["AvatarPath"].Value = VkAvatarPath;

            //HttpContext.Response.Cookies["AvatarPath"].Value = DefaultAvatarPath; 

            using (var context = new ApplicationContext())
            {
                //1. в AspNetUserLogins ищем ID страницы ВК.
                var userAlreadyExist = (from b in context.Logins
                                        where b.LoginProvider == VkLoginProvider == true
                                        select b).FirstOrDefault();

                if (userAlreadyExist != null)//если есть инфа в доп табл, тогда будет инфа и в гл табл, поэтому логинимся.
                {
                    ApplicationUser user = UserManager.Find(new UserLoginInfo(VkLoginProvider, VkProviderKey)); //ищем конкретного юзера в гл табл по данным доп табл! ("3651939", "VK");
                    HttpContext.Response.Cookies["first_name_last_name"].Value = user.MyUserName;

                    //логиним найденного юзера.
                    ClaimsIdentity claim = UserManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = true }, claim);

                    string pathView = HttpContext.Request.Cookies["pathView"].Value; 
                    
                    return Redirect(pathView); //вернутся на страницу с которой пришли
                }
                else //Если есть данные в осн таблице, но нету id VK в доп табл, тогда создать юзера!
                {
                    //1. Качаем картинку по VkAvatarUrl и сохраняем в папке на компе.
                    using (WebClient client = new WebClient())
                    {
                        client.DownloadFile(VkAvatarUrl, VkAvatarPath);
                    }

                    byte[] VkAvatarDB = GetImageFromFileToByteArray(VkAvatarPath); //2. сохранили скачанную из паки на Пк фотку в БД.

                    VkLoginConfirmEmailModel myVkLoginConfirmEmailModel = new VkLoginConfirmEmailModel();
                    myVkLoginConfirmEmailModel.idVkUser = VkLoginProvider;
                    myVkLoginConfirmEmailModel.Email = VkEmail;
                    myVkLoginConfirmEmailModel.MyUserName = VkMyUserName;
                    myVkLoginConfirmEmailModel.ImageDataMy = VkAvatarDB;

                    return View("~/Views/Account/ThirdParty_VK/VkLoginConfirmEmail.cshtml", myVkLoginConfirmEmailModel);
                }
            }
        }

        //Когда заходим через ВК, то нужно указать Email, по которому будет привязка в базе.
        //Так же рассмотрен случай, когда уже есть учетка по Email, и будет предложено привязать в ней свою учетку ВК!
        [AllowAnonymous]
        public ActionResult VkLoginConfirmEmail()
        {
            return View("~/Views/Account/ThirdParty_VK/VkLoginConfirmEmail.cshtml");
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult VkLoginConfirmEmail(VkLoginConfirmEmailModel vkModel) //2. Если нету пользователя, тогда создаем
        {
            if (ModelState.IsValid)
            {
                // // // Проверки на уникальность:
                //1. Проверочка, если пытаемся ввести уже существующий E-mail!!!
                ApplicationUser userAlreadyConfirmEmail = UserManager.FindByEmail(vkModel.Email);
                if (userAlreadyConfirmEmail != null) // если есть!
                {
                    ModelState.AddModelError("Email", "Такой Email уже есть базе!!");
                    return View("~/Views/Account/ThirdParty_VK/VkLoginConfirmEmail.cshtml", vkModel);
                }

                //*если поле никнейма - пустое, тогда по дефолту присвоить никнейму - емейл адрес!
                string tempVkMyUserName = vkModel.MyUserName;
                if (tempVkMyUserName == string.Empty)
                    tempVkMyUserName = vkModel.Email;

                //2.1. Создаем нового пользователя.  
                ApplicationUser newUser = new ApplicationUser { Email = vkModel.Email, UserName = vkModel.Email, MyUserName = tempVkMyUserName }; //Создаем пользователя с одинаковыми полями Email и UserName, тк. по дефолту UserName - должен быть уникальным полем и часть функций OWIN заточены под UserName! Для Никнейма проще создать новое поле.
                HttpContext.Response.Cookies["first_name_last_name"].Value = tempVkMyUserName;

                IdentityResult result = UserManager.Create(newUser);
                if (result.Succeeded) //2.2. Если создаст пользователя успешно, добавим данные в доп таблицу!
                {
                    using (var context = new ApplicationContext())
                    {
                        //3.1. Допишем данные в доп табл AspNetUserLogins!
                        ApplicationUserLogins identityUserLogin = new ApplicationUserLogins();
                        identityUserLogin.UserId = newUser.Id; //берем из таблицы AspNetUser ID-поле для нового пользователя и поместим его в UserId доп табл.
                        identityUserLogin.LoginProvider = vkModel.idVkUser; //id VK = "3651939" 
                        identityUserLogin.ProviderKey = VkProviderKey;     //"VK"

                        identityUserLogin.ImageDataMy = vkModel.ImageDataMy; //картинка

                        context.Logins.Add(identityUserLogin);  //Добавим новый объект с данными к таблице AspNetUserLogins
                        context.SaveChanges(); //сохраним добро
                    }

                    //3.2. теперь логиним пользователя
                    ClaimsIdentity claim = UserManager.CreateIdentity(newUser, DefaultAuthenticationTypes.ApplicationCookie);
                    AuthenticationManager.SignOut(); // удаляет авторизационные куки.
                    AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = true }, claim); // создает авторизационные куки.

                    string pathView = HttpContext.Request.Cookies["pathView"].Value;//вернутся на страницу с которой пришел
                    return Redirect(pathView);
                }
                else //не создаст Юзера
                {
                    foreach (string error in result.Errors)
                        ModelState.AddModelError("", error);
                    return View("~/Views/Account/ThirdParty_VK/VkLoginConfirmEmail.cshtml");
                }
            }
            else
                return View("~/Views/Account/ThirdParty_VK/VkLoginConfirmEmail.cshtml");
        }

        // // // для фоток 
        //1. Из директории сайта грузим картинку в байтовый массив в бд.
        public static byte[] GetImageFromFileToByteArray(string filePath)
        {
            using (FileStream stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                using (BinaryReader reader = new BinaryReader(stream))
                {
                    byte[] photo = reader.ReadBytes((int)stream.Length);
                    return photo;
                }
            }
        }

        //метод отображает картинку на главной!
        public ActionResult GetImageFromDB() //http://www.cyberforum.ru/asp-net-mvc/thread617793.html
        {
            string path = HttpContext.Request.Cookies["AvatarPath"].Value;
            {
                if (!string.IsNullOrEmpty(path))
                {
                    FileInfo file = new FileInfo(path);
                    if (file.Exists)
                        return File(file.FullName, "text/plain", file.Name);
                }
                return Content("");
            }
        }
    }
}




